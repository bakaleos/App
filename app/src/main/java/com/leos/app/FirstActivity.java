package com.leos.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class FirstActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_layout);
        //===========================================                                   ============================
        Button button1 = findViewById(R.id.button_1);                                   // 1.New button
        button1.setOnClickListener(new View.OnClickListener() {                         // 2.button OnClickListener
            @Override
            public void onClick(View v) {                                               // 3.onClick method Override
                Toast.makeText(FirstActivity.this,                              // 4.Toast.makeText
                        "Rua!",                                                    //==========================
                        Toast.LENGTH_SHORT).show();
                //===========================================

                // intent to SecondActivity
                Intent intent = new Intent(FirstActivity.this, ThirdActivity.class);
                startActivity(intent);

                //=========================
                // intent to other activity
                /*
                Intent intent = new Intent("com.example.activitytest.ACTION_START");
                intent.addCategory("com.example.activitytest.MY_CATEGORY");
                startActivity(intent);
                */

                //=========================
                // intent to system-browser
                /*
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("https://www.baidu.com"));
                startActivity(intent);
*/
                //===========================================
            }
        });
        //===========================================
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //===========================================
        // Menu
        // toast
        switch (item.getItemId()) {
            case R.id.add_item:
                Toast.makeText(this, "You clicked Add", Toast.LENGTH_SHORT).show();
                break;
            case R.id.remove_item:
                Toast.makeText(this, "You clicked Remove", Toast.LENGTH_SHORT).show();
                break;
            case R.id.Exit:
                Toast.makeText(this, "Bye", Toast.LENGTH_SHORT).show();
                finish();
                break;
            default:
        }
        return true;
        //===========================================
    }
}