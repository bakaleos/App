package com.leos.app;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_layout);
        //===========================================
        Button button2 = findViewById(R.id.button_2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Toast

/*                Toast.makeText(SecondActivity.this,
                        "Exit this Activity",
                        Toast.LENGTH_SHORT).show();*/

                // finish this activity
                finish();
            }
        });
        //===========================================
    }
}
